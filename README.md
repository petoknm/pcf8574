[![Build Status](https://travis-ci.org/petoknm/PCF8574.svg?branch=master)](https://travis-ci.org/petoknm/PCF8574)

# PCF8574
Driver for PCF8574 I2C IO expander for STM32 microcontrollers.

## TODO:
 - Documentation
 - More examples
 - Photos
 - v1.0.0

## Documentation
You can find [generated documentation here](./api.md).

## Example
Check out the [general example](./example/general)
which is used by every other microcontroller specific example.

## Requirements
For building you will need:
 - cmake
 - make
 - arm-none-eabi-gcc (5.4, 7.3.0)

For developing you will also need:
 - clang-format
 - doxygen
 - graphviz
 - moxygen

## Building
```shell
mkdir build && cd build
# Using arm-none-eabi-gcc
cmake -DCMAKE_TOOLCHAIN_FILE=../cmake_toolchains/arm-gcc-toolchain.cmake .. && make
```
Binaries can then be found in the `build/example/$EXAMPLE` folder.

## Updating documentation
```shell
make docs
```

## Formatting sources
```shell
make format
```
