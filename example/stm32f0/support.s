	.syntax unified
	.text
	.thumb
	.cpu cortex-m0

    .thumb_func
    .global __aeabi_idiv0
__aeabi_idiv0:

    .thumb_func
    .global __aeabi_uidiv
__aeabi_uidiv:

    .thumb_func
    .global __aeabi_uidivmod
__aeabi_uidivmod:

.Luidivmod:
    cmp	r1, #0
    bne	1f
    b	__aeabi_idiv0

1:
    movs	r2, #1
    movs	r3, #0
    cmp	r0, r1
    bls	.Lsub_loop
    adds	r1, #0
    bmi	.Lsub_loop

.Ldenom_shift_loop:
    lsls	r2, #1
    lsls	r1, #1
    bmi	.Lsub_loop
    cmp	r0, r1
    bhi	.Ldenom_shift_loop

.Lsub_loop:
    cmp	r0, r1
    bcc	.Ldont_sub

    subs	r0, r1
    orrs	r3, r2

.Ldont_sub:
    lsrs	r1, #1
    lsrs	r2, #1
    bne	.Lsub_loop

    mov	r1, r0
    mov	r0, r3
    bx	lr
