#include "example.h"
#include "example_defs.h"

static void error() {
    while (1) {}
}

void example() {
    pcf8574 pcf;

    pcf.address = 7;
    pcf.timeout = 1000;
    pcf.i2c     = I2C;

    if (pcf8574_init(&pcf) != PCF8574_OK) error();

    while (1) {
        uint8_t val;
        if (pcf8574_read(&pcf, &val) != PCF8574_OK) error();
        val++;
        if (pcf8574_write(&pcf, val) != PCF8574_OK) error();
        HAL_Delay(100);
    }
}
